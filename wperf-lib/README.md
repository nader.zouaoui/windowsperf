# wperf-lib

The `wperf-lib` is our WindowsPerf "C" bindings library, providing access to the basic functionality of WindowsPerf via a C API. Please note that not all features of WindowsPerf are implemented in this library.

## Components

- [wperf-lib-app](https://gitlab.com/Linaro/WindowsPerf/windowsperf/-/tree/main/wperf-lib-app) is an example application linked with `wperf-lib`.
- [wperf-lib-c-compat](https://gitlab.com/Linaro/WindowsPerf/windowsperf/-/tree/main/wperf-lib-app/wperf-lib-c-compat) is smoke test application for `wperf-lib`.
- [wperf-lib-timeline](https://gitlab.com/Linaro/WindowsPerf/windowsperf/-/tree/main/wperf-lib-app/wperf-lib-timeline) is smoke test application for `wperf-lib`.
