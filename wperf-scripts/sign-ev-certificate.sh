#!/bin/bash

#
# This scrip signs all the appropriate files in <release_directory>
#

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $(basename "$0") <server> <release_directory>"
    exit 1
fi

# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

print_yellow() {
    echo -e "${YELLOW}$1${NC}"
}

print_green() {
    echo -e "${GREEN}$1${NC}"
}

print_red() {
    echo -e "${RED}$1${NC}"
}

print_white() {
    echo -e "${WHITE}$1${NC}"
}

SERVER_NAME="$1"
RELEASE_DIRECTORY="$2"

check_if_signed() {
    FILE_NAME="$1"
    SIGNED=$(sigcheck -i "$FILE_NAME" | grep Verified | grep Signed | wc -l)
    if [ "$SIGNED" -eq 1 ]; then
        print_green "Signed $FILE_NAME"
    else
        print_red "Signing failed for $FILE_NAME !!!"
    fi
}

sign_file() {
    f="$1"  # Filename

    print_yellow "Processing file: '$f'..."

    # Find place on remote server:
    TEMP_DIR=$(ssh "$SERVER_NAME" mktemp -d)

    # Copy to remote service file to sign
    FILE_NAME=$(realpath "$f")
    print_green "$FILE_NAME -> $SERVER_NAME:$TEMP_DIR/"
    scp "$FILE_NAME" "$SERVER_NAME:$TEMP_DIR/"

    REMOTE_FILE="$TEMP_DIR/$(basename "$f")"
    print_yellow "Signing: $REMOTE_FILE"
    ssh "$SERVER_NAME" "~/driver_signing/linaro-ev-cert.sh" "$REMOTE_FILE" "$REMOTE_FILE"

    print_green "$SERVER_NAME:$REMOTE_FILE -> $FILE_NAME"
    scp "$SERVER_NAME:$REMOTE_FILE" "$FILE_NAME"

    if [[ "${FILE_NAME##*.}" != "cat" ]]; then
        check_if_signed "$FILE_NAME"
    fi

    print_green "Cleaning $REMOTE_FILE ..."
    ssh "$SERVER_NAME" rm -v "$REMOTE_FILE"
}

for f in $(find "$RELEASE_DIRECTORY" -type f \( -name "*.exe" -o -name "*.sys" -name "*.cat" \) -print);
do
    sign_file "$f"
done

#
# *** Package CAB file in order to sign the driver ***
#

# Define a multiline string with DDF file content using Heredoc
DDF_FILE_CONTENT=$(cat <<EOF
.OPTION EXPLICIT     ; Generate errors
.Set CabinetFileCountThreshold=0
.Set FolderFileCountThreshold=0
.Set FolderSizeThreshold=0
.Set MaxCabinetSize=0
.Set MaxDiskFileCount=0
.Set MaxDiskSize=0
.Set CompressionType=MSZIP
.Set Cabinet=on
.Set Compress=on
;Specify file name for new cab file
.Set CabinetNameTemplate=wperf-driver.cab
; Specify the subdirectory for the files. 
; Your cab file should not have files at the root level,
; and each driver package must be in a separate subfolder.
.Set DestinationDir=wperf-driver
;Specify files to be included in cab file
EOF
)

# Change to the target directory and execute the Cab package commands
if cd "$RELEASE_DIRECTORY"; then
    DDF_FILENAME="wperf-driver.ddf"

    echo "$DDF_FILE_CONTENT" > "$DDF_FILENAME"
    for f in $(find . -type f \( -name "*.pdb" -o -name "*.sys" -o -name "*.inf" -o -name "*.cat" \) -print);
    do
        FILE_NAME=$(realpath "$f")
        # -a, --absolute        output absolute path
        # -w, --windows         print Windows form of NAMEs (C:\WINNT)
        # -l, --long-name       print Windows long form of NAMEs (with -w, -m only)
        echo $(cygpath -a -w -l "$FILE_NAME") >> "$DDF_FILENAME"
    done

    makecab //f "$DDF_FILENAME"
else
    print_red "Failed to change directory to $RELEASE_DIRECTORY"
fi

#
# Sign CAB file
#

CAB_FILE=$(find disk1/ -type f \( -name "*.cab" \) -print)
print_yellow "Signing: $CAB_FILE"
sign_file "$CAB_FILE"
check_if_signed "$CAB_FILE"
