#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2024, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Module is testing `wperf record` with CPython executables in debug mode."""
import os
import pytest
from common import run_command, check_if_file_exists
from common_cpython import CPYTHON_EXE_DIR

def test_cpython_bench_record_time():
    """ Test trivial stdout printouts for `wperf record` command. """
    python_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(python_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {python_d_exe_path}")

    cmd = f"wperf record -c 0 -e ld_spec:100000 --timeout 5 -- {python_d_exe_path} -c 10**10**100"
    stdout, _ = run_command(cmd)

    #
    #    Example output for sampling:
    #
    #   >wperf record -c 0 -e ld_spec:100000 --timeout 5 -- cpython\PCbuild\arm64\python_d.exe -c 10**10**100
    #   base address of 'cpython\PCbuild\arm64\python_d.exe': 0x7ff7cbe51288, runtime delta: 0x7ff68be50000
    #   sampling .....e.. done!
    #   ======================== sample source: ld_spec, top 50 hot functions ========================
    #           overhead  count  symbol
    #           ========  =====  ======
    #              81.45    417  x_mul:python312_d.dll
    #               4.49     23  _Py_atomic_load_32bit_impl:python312_d.dll
    #               4.30     22  v_isub:python312_d.dll
    #               2.93     15  PyErr_CheckSignals:python312_d.dll
    #               1.76      9  x_add:python312_d.dll
    #               1.56      8  unknown
    #               1.37      7  v_iadd:python312_d.dll
    #               0.59      3  _Py_atomic_load_64bit_impl:python312_d.dll
    #               0.39      2  _Py_ThreadCanHandleSignals:python312_d.dll
    #               0.20      1  _Py_DECREF_INT:python312_d.dll
    #               0.20      1  _PyMem_DebugCheckAddress:python312_d.dll
    #               0.20      1  read_size_t:python312_d.dll
    #               0.20      1  _PyMem_DebugRawAlloc:python312_d.dll
    #               0.20      1  _PyObject_Malloc:python312_d.dll
    #               0.20      1  _PyErr_CheckSignalsTstate:python312_d.dll
    #             100.00%   512  top 15 in total
    #
    #                  5.456 seconds time elapsed

    #
    # Sampling standard output smoke tests
    #
    assert b'sampling ...' in stdout
    assert b'sample source: ld_spec' in stdout
    assert b'seconds time elapsed' in stdout
