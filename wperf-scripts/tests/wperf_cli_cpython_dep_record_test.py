#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2024, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Module is testing `wperf record` with CPython executables in debug mode."""
import os
import json
from statistics import median
from time import sleep
import pytest
from jsonschema import validate
from common import run_command, is_json, check_if_file_exists, get_schema
from common_cpython import CPYTHON_EXE_DIR


### Test cases

@pytest.mark.parametrize("event,event_freq,hot_symbol,hot_minimum,python_arg",
[
    ("ld_spec", 10000, "x_mul:python313_d.dll", 65, "10**10**100"),
    ("dp_spec", 10000, "write_size_t:python313_d.dll", 10, "list(range(100000000))"),
]
)
def test_cpython_bench_record_hotspot(event, event_freq, hot_symbol, hot_minimum, python_arg):
    """ Test `wperf record` for python_d.exe call for example `Googolplex` calculation.
        We will sample for one event + sampling frequency and we expect one hottest symbol
        with some minimum sampling %."""

    ## Execute benchmark
    python_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(python_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {python_d_exe_path}")

    overheads = []  # Results of sampling of the symbol
    #
    # Run test N times and check for media sampling output
    #
    for _ in range(3):
        sleep(2)    # Cool-down the core

        cmd = ["wperf", "record",
                "-e", f"{event}:{event_freq}",
                "-c", "7",
                "--timeout", "5",
                "--json",
                "--", f"{python_d_exe_path}", "-c", f"{python_arg}"]
        stdout, _ = run_command(cmd)

        # Sanity checks
        assert is_json(stdout), f"in {cmd}"
        json_output = json.loads(stdout)

        # Check sampling JSON output for expected functions
        assert "sampling" in json_output
        assert "events" in json_output["sampling"]

        events = json_output["sampling"]["events"]  # List of dictionaries, for each event
        assert len(events) == 1  # We expect one record for e.g. `ld_spec`

        evt = json_output["sampling"]["events"][0]  # e.g. `ld_spec` data
        assert event in evt["type"]

        samples = evt["samples"]

        def find_sample(samples, symbol):
            """ Find sample in samples and return it when we can find it for given `symbol`.
            See:

            "events": [
                {
                    "type": "ld_spec",
                    "samples": [
                        {
                            "overhead": 81.6406,
                            "count": 418,
                            "symbol": "x_mul:python313_d.dll"
                        },
                        {
                            "overhead": 4.88281,
                            "count": 25,
                            "symbol": "v_iadd:python313_d.dll"
                        },
            """
            for sample in samples:
                if sample["symbol"] == symbol:
                    return sample
            return None

        # {
        #     "overhead": 81.6406,
        #     "count": 418,
        #     "symbol": "x_mul:python313_d.dll"
        # },

        # We expect in events.samples[0] hottest sample (which we want to check for)
        hotest_symbol = samples[0]
        symbol_overhead = hotest_symbol["overhead"]
        symbol_count = hotest_symbol["count"]
        symbol_name = hotest_symbol["symbol"]

        if find_sample(samples, hot_symbol) is False:
            pytest.skip(f"{python_d_exe_path} hottest function sampled: '{symbol_name}' count={symbol_count} overhead={symbol_overhead}, expected '{hot_symbol}' -- sampling mismatch")

        evt_sample = find_sample(samples, hot_symbol)
        assert evt_sample is not None, f"Can't find `{hot_symbol}` symbol in sampling output!"
        overheads.append(evt_sample["overhead"])    # Store results for median calculation

    #
    # We want to see at least e.g. 70% of samples in e.g `x_mul`:
    #
    assert len(overheads), f"overheads={overheads}, cmd='{cmd}'"
    assert median(overheads) >= hot_minimum, f"expected {hot_minimum}% sampling hotspot in {hot_symbol}, overheads={overheads}, cmd='{cmd}'"


@pytest.mark.parametrize("event,event_freq,python_arg,file_path",
[
    ("ld_spec", 10000, "10**10**100", "test-disassemble-1.json"),
    ("st_spec", 10000, "list(range(100000000))", "test-disassemble-2.json"),
]
)
def test_cpython_bench_record_disassemble(event, event_freq, python_arg, file_path):
    """ Test `wperf record` with `--disassemble` feature.
        We will check basic presence of disassembly output in JSON.
    """

    ## Execute benchmark
    python_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(python_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {python_d_exe_path}")

    cmd = ["wperf", "record",
            "-e", f"{event}:{event_freq}",
            "-c", "5",
            "--timeout", "10",
            "--disassemble",    # Important part!
            "--json",
            "--output", f"{file_path}",
            "--", f"{python_d_exe_path}", "-c", f"{python_arg}"]
    _, _ = run_command(cmd)

    json_output = {}
    with open(str(file_path)) as json_file:
        json_output = json.loads(json_file.read())

    # Check sampling JSON output for expected functions
    assert "sampling" in json_output
    assert "events" in json_output["sampling"]

    events = json_output["sampling"]["events"]  # List of dictionaries, for each event
    assert len(events) == 1  # We expect one record for e.g. `ld_spec`
    assert event in events[0]["type"]   # e.g. `ld_spec` data

    #
    # Check if annotate and disassembly are present in output.
    # Smoke test annotate and disassemble output:
    #
    for event_elem in events:
        assert "annotate" in event_elem
        annotate = event_elem["annotate"]

        for annotate_elem in annotate:
            assert "source_code" in annotate_elem
            source_code = annotate_elem["source_code"]

            for source_code_elem in source_code:
                assert "disassembled_line" in source_code_elem
                assert "disassemble" in source_code_elem["disassembled_line"]
                disassemble = source_code_elem["disassembled_line"]["disassemble"]
                assert len(disassemble)

                for disassemble_elem in disassemble:
                    assert "address" in disassemble_elem
                    assert "instruction" in disassemble_elem

                    assert len(disassemble_elem["address"])
                    assert len(disassemble_elem["instruction"])



def test_cpython_bench_record_time():
    """ Test if we print `seconds time elapsed` with sampling.  """
    python_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(python_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {python_d_exe_path}")

    cmd = ["wperf", "record",
            "-e", "ld_spec:100000",
            "-c", "1",
            "--timeout", "1",
            "--annotate",
            "--", f"{python_d_exe_path}", "-c", "10**10**100"]
    stdout, _ = run_command(cmd)

    assert b'seconds time elapsed' in stdout


@pytest.mark.parametrize("flag",
[
    ("--annotate"),
    ("--disassemble"),
]
)
def test_cpython_bench_record_json_schema(request, tmp_path, flag):
    """ Test for `wperf record`JSON output matches schema. """

    python_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")
    test_path = os.path.dirname(request.path)
    file_path = tmp_path / 'test.json'

    if not check_if_file_exists(python_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {python_d_exe_path}")

    cmd = ["wperf", "record",
            "-e", "ld_spec:100000",
            "-c", "2",
            "--timeout", "10",
            "--json",
            f"{flag}",
            "--output", f"{str(file_path)}",
            "--", f"{python_d_exe_path}", "-c", "10**10**100"]
    _, _ = run_command(cmd)

    try:
        with open(str(file_path)) as json_file:
            json_output = json.loads(json_file.read())
        validate(instance=json_output, schema=get_schema("sample", test_path))
    except Exception as err:
        assert False, f"Unexpected {err=}, {type(err)=}, cmd='{cmd}'"


@pytest.mark.parametrize("arg",
[
   ("^"),       # All strings start with empty string
   ("$"),       # All strings end with empty string
   ("x_mul"),
   ("^x_"),
   ("^x_mul"),
   ("x_mul$"),
   ("_mul$"),
   ("X_MUL"),
   ("^x_mul$"),
]
)
def test_cpython_bench_record_symbol(arg):
    """ Test sampling filtering for record with `-s <symbol>` """
    python_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(python_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {python_d_exe_path}")

    cmd = ["wperf", "record",
            "-e", "ld_spec:100000",
            "-c", "1",
            "--symbol", f"{arg}",
            "--timeout", "3",
            "--", f"{python_d_exe_path}", "-c", "10**10**100"]
    stdout, _ = run_command(cmd)

    assert b'x_mul' in stdout
    assert b'x_mul:python' in stdout
