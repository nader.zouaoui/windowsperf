#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2025, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


""" Module is testing `wperf cpuinfo` feature.
"""

import json
from common import run_command, is_json, check_if_file_exists

### Test cases

def test_wperf_test_cpuinfo_json():
    """ Test `wperf test` JSON output  """
    cmd = 'wperf cpuinfo --json'
    stdout, _ = run_command(cmd.split())
    assert is_json(stdout)


def test_wperf_test_cpuinfo_json_file_output_exists(tmp_path):
    """ Test `wperf test` JSON output to file"""
    file_path = tmp_path / 'test.json'
    cmd = ['wperf', 'cpuinfo', '--output', str(file_path)]
    _, _ = run_command(cmd)
    assert check_if_file_exists(str(file_path))


def test_wperf_test_cpuinfo_json_file_output_valid(tmp_path):
    """ Test `wperf test` JSON output to file validity """
    file_path = tmp_path / 'test.json'
    cmd = ['wperf', 'cpuinfo', '--output', str(file_path)]
    _, _ = run_command(cmd)

    try:
        with open(file_path) as f:
            json_obj = f.read()
            assert is_json(json_obj)
    except:
        assert 0


def test_wperf_test_cpuinfo_json_smoke_test(tmp_path):
    """ Test basic `wperf cpuinfo` output. """
    file_path = tmp_path / 'test.json'
    cmd = ['wperf', 'cpuinfo', '--json']
    stdout, _ = run_command(cmd)

    assert is_json(stdout)
    json_output = json.loads(stdout)

    assert "CPU_Cores_Information" in json_output
    cpu_cores_information = json_output["CPU_Cores_Information"]
    assert len(cpu_cores_information), "no CPU cores information in 'CPU_Cores_Information', no cores detected!"

#   {
#       "Core": "0",
#       "Name": "neoverse-n1",
#       "Arch": "armv8.2",
#       "PMU_Ver": "pmu_v3",
#       "Description": "Neoverse N1",
#       "MIDR_EL1": "0x000000000000413fd0c1"
#   },

    cores = []  # Accumulate core numbers as integers
    for core_info in cpu_cores_information:
        cores.append(int(core_info.get("Core")))    # Must be integer
        assert core_info.get("MIDR_EL1")    # MIDR_EL1 should be always available, CPU core name may not be!

    #
    # Check if number of unique cores is equal to number
    # of cores reported in "CPU_Cores_Information"
    #
    assert len(set(cores)) == len(cpu_cores_information)
