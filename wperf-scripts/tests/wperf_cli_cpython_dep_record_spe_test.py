#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2024, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module provides a comprehensive suite of tests for the WindowsPerf,
targeting all command line options related to the SPE feature set, using the
CPython executable for sampling.
The tests ensure that the WindowsPerf behaves as expected when using
SPE-related options with CPython.
"""

import os
import json
from statistics import median
from time import sleep
import pytest
from jsonschema import validate
from common import run_command, is_json, check_if_file_exists, get_schema
from common import get_spe_version, wperf_event_is_available
from common_cpython import CPYTHON_EXE_DIR

#
# Skip whole module if SPE is not supported in this configuration
#

# Skip whole module if ustress is not supported by this CPU
spe_device = get_spe_version()
assert spe_device is not None
if not spe_device.startswith("FEAT_SPE"):
    pytest.skip(f"unsupported configuration: no SPE support in HW, see spe_device.version_name={spe_device}",
        allow_module_level=True)

## Is SPE enabled in `wperf` CLI?
if not wperf_event_is_available("arm_spe_0//"):
    pytest.skip(f"unsupported configuration: no SPE support in `wperf`, see spe_device.version_name={spe_device}",
        allow_module_level=True)

### Test cases


@pytest.mark.parametrize("spe_filters,hot_symbol,hot_minimum,python_arg",
[
    ("",                           "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1",              "x_mul:python313_d.dll", 65, "10**10**100"),

    # `ts_enable` filter
    ("ts_enable=1",                "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,ts_enable=1",  "x_mul:python313_d.dll", 65, "10**10**100"),

    # `jitter` + `period` filters
    ("load_filter=1,jitter=1",              "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,period=0",              "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,period=1024",           "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,period=4096",           "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,jitter=1,period=0",     "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,jitter=1,period=1024",  "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,jitter=1,period=4096",  "x_mul:python313_d.dll", 65, "10**10**100"),
]
)
def test_cpython_bench_spe_hotspot(request, tmp_path, spe_filters, hot_symbol, hot_minimum, python_arg):
    """ Test `wperf record` for python_d.exe call for example `Googolplex` calculation.
        We will sample with SPE for one event + filters and we expect one hottest symbol
        with some minimum sampling %."""
    ## Execute benchmark
    pyhton_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(pyhton_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {pyhton_d_exe_path}")

    test_path = os.path.dirname(request.path)
    file_path = tmp_path / 'test.json'

    overheads = []  # Results of sampling of the symbol
    core_no = 9

    for _ in range(3):
        #
        # Run test N times and check for media sampling output
        #
        sleep(2)    # Cool-down the core

        json_file = str(file_path)
        cmd = ["wperf", "record",
                "-e", f"arm_spe_0/{spe_filters}/",
                "-c", f"{core_no}",
                "--timeout", "30",
                "--json",
                "--output", f"{json_file}",
                "--", f"{pyhton_d_exe_path}", "-c", f"{python_arg}"]

        _, _ = run_command(cmd)
        core_no += 1

        # Load JSON from output file
        json_output = {}

        # load it as an object
        with open(str(file_path)) as json_file:
            json_str = json_file.read()
            assert is_json(json_str), f"in {cmd}, json_file='{json_file}'"
            json_output = json.loads(json_str)

        # Check sampling JSON output for expected functions
        assert "sampling" in json_output
        assert "counting" in json_output
        assert "sampling" in json_output["sampling"]
        assert "events" in json_output["sampling"]["sampling"]

        events = json_output["sampling"]["sampling"]["events"]  # List of dictionaries, for each event
        assert len(events) >= 1  # We expect at least one record

        """
        "events": [
            {
                "type": "BRANCH_OR_EXCEPTION-UNCONDITIONAL-DIRECT/retired",
                "samples": [
                    {
                        "overhead": 100,
                        "count": 7,
                        "symbol": "x_mul:python313_d.dll"
                    }
                ],
                "interval": 0,
                "printed_sample_num": 1,
                "annotate": []
            },
        """
        for event in events:    # Gather all symbol overheads for all events for given symbol
            for sample in event["samples"]:
                if sample["symbol"] == hot_symbol:
                    overheads.append(int(sample["overhead"]))

    #
    # We want to see at least e.g. 70% of samples in e.g `x_mul`:
    #
    assert len(overheads), f"overheads={overheads}, cmd='{cmd}'"
    assert median(overheads) >= hot_minimum, f"expected {hot_minimum}% SPE sampling hotspot in {hot_symbol}, overheads={overheads}, cmd='{cmd}'"


@pytest.mark.parametrize("spe_filters,hot_symbol,hot_minimum,python_arg",
[
    ("ts_enable=1",             "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1",           "x_mul:python313_d.dll", 65, "10**10**100"),
    ("load_filter=1,jitter=1",  "x_mul:python313_d.dll", 65, "10**10**100"),
]
)
def test_cpython_bench_spe_period_test(spe_filters, hot_symbol, hot_minimum, python_arg):
    """ Test `wperf record` for python_d.exe call for example `Googolplex` calculation.
        We will sample process to make sure that we get less and less SPE records as we
        increase `period`.
    """
    ## Execute benchmark
    pyhton_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(pyhton_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {pyhton_d_exe_path}")

    sample_feed = []        # SPE PMU event that should increase with `period` decreasing
    sample_filtrate = []    # SPE PMU event that should increase with `period` decreasing

    # By increasing `period` we should receive less and less
    # of SPE `sample_feed` and `sample_filtrate` PMU events
    for period in [1024, 4096, 8192]:
        # Run test N times and check for media sampling output
        sleep(2)    # Cool-down the core

        cmd = ["wperf", "record",
                "-e", f"arm_spe_0/{spe_filters},period={period}/",
                "-c", "13",
                "--timeout", "12",
                "--json",
                "--", f"{pyhton_d_exe_path}", "-c", f"{python_arg}"]

        stdout, _ = run_command(cmd)

        # Sanity checks
        assert is_json(stdout), f"in {cmd}"
        json_output = json.loads(stdout)

        # Check sampling JSON output for expected functions
        assert "counting" in json_output
        assert "core" in json_output["counting"]
        assert "cores" in json_output["counting"]["core"]

        #
        # We will inspect the only core we've specified
        #
        cores = json_output["counting"]["core"]["cores"]
        assert len(cores) == 1   # Only core specified with `-c`
        assert len(cores[0]["Performance_counter"]) >= 4     # 4x SPE PMU counter and fixed counter

        for Performance_counter in cores[0]["Performance_counter"]:
            # Let's store `sample_feed` and `sample_filtrate`
            if Performance_counter["event_name"] == "sample_feed":
                sample_feed.append(Performance_counter["counter_value"])

            if Performance_counter["event_name"] == "sample_filtrate":
                sample_filtrate.append(Performance_counter["counter_value"])

    assert sample_feed == sorted(sample_feed, reverse=True), f"sample_feed={sample_feed}"
    assert sample_filtrate == sorted(sample_filtrate, reverse=True), f"sample_filtrate={sample_filtrate}"


@pytest.mark.parametrize("verbose",
[
    ("-v"),
    (""),
]
)
@pytest.mark.parametrize("event,spe_filters,python_arg",
[
    ("arm_spe_0", "",              "10**10**100"),
    ("arm_spe_0", "load_filter=1", "10**10**100"),
]
)
def test_cpython_bench_spe_json_schema(request, tmp_path, verbose, event, spe_filters, python_arg):
    """ Test SPE JSON output against scheme """
    ## Execute benchmark
    pyhton_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(pyhton_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {pyhton_d_exe_path}")

    test_path = os.path.dirname(request.path)
    file_path = tmp_path / 'test.json'

    cmd = ["wperf", "record",
            "-e", f"{event}/{spe_filters}/",
            "-c", "2",
            f"{verbose}",
            "--timeout", "5",
            "--output", f"{str(file_path)}",
            "--", f"{pyhton_d_exe_path}", "-c", f"{python_arg}"]
    cmd = list(filter(None, cmd))   # Remove empty string (e.g. {verbose} can be empty) from the command line list

    _, _ = run_command(cmd)

    json_output = {}

    try:
        with open(str(file_path)) as json_file:
            json_output = json.loads(json_file.read())
        validate(instance=json_output, schema=get_schema("spe", test_path))
    except Exception as err:
        assert False, f"Unexpected {err=}, {type(err)=}, cmd='{cmd}'"

@pytest.mark.parametrize("verbose",
[
    ("-v"),
    (""),
]
)
@pytest.mark.parametrize("event,spe_filters,python_arg",
[
    ("arm_spe_0", "",              "10**10**100"),
    ("arm_spe_0", "load_filter=1", "10**10**100"),
]
)
def test_cpython_bench_spe_json_stdout_schema(request, tmp_path, verbose, event, spe_filters, python_arg):
    """ Test SPE JSON output against stdout scheme """
    ## Execute benchmark
    pyhton_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(pyhton_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {pyhton_d_exe_path}")

    test_path = os.path.dirname(request.path)

    cmd = ["wperf", "record",
            "-e", f"{event}/{spe_filters}/",
            "-c", "3",
            f"{verbose}",
            "--timeout", "5",
            "--json",
            "--", f"{pyhton_d_exe_path}", "-c", f"{python_arg}"]
    cmd = list(filter(None, cmd))   # Remove empty string (e.g. {verbose} can be empty) from the command line list

    stdout, _ = run_command(cmd)

    json_output = json.loads(stdout)

    try:
        validate(instance=json_output, schema=get_schema("spe", test_path))
    except Exception as err:
        assert False, f"Unexpected {err=}, {type(err)=}, cmd='{cmd}'"

@pytest.mark.parametrize("spe_filters,python_arg",
[
    ("",                   "10**10**100"),
    ("load_filter=1",      "10**10**100"),
    ("load_filter=1,st=1", "10**10**100"),
]
)
def test_cpython_bench_spe_consistency(request, tmp_path, spe_filters, python_arg):
    """ Test SPE JSON output against stdout scheme """
    ## Execute benchmark
    pyhton_d_exe_path = os.path.join(CPYTHON_EXE_DIR, "python_d.exe")

    if not check_if_file_exists(pyhton_d_exe_path):
        pytest.skip(f"Can't locate CPython native executable in {pyhton_d_exe_path}")

    cmd = ["wperf", "record",
            "-e", f"arm_spe_0/{spe_filters}/",
            "-c", "2",
            "--timeout", "30",
            "--json",
            "--", f"{pyhton_d_exe_path}", "-c", f"{python_arg}"]

    stdout, _ = run_command(cmd)

    json_output = json.loads(stdout)

    total_hits = 0
    for event in json_output["sampling"]["sampling"]["events"]:
        event_hits = 0
        #assert False, f"{event["samples"]=}"
        for sample in event["samples"]:
            event_hits = event_hits + sample["count"]
        total_hits = total_hits + event_hits

    #
    # Check if `samples_generated` and `samples_dropped` match SPE PMU event values in counting
    #
    samples_generated = json_output["sampling"]["sampling"]["samples_generated"]
    samples_dropped = json_output["sampling"]["sampling"]["samples_dropped"]

    sample_filtrate = 0

    for events in json_output["counting"]["core"]["cores"][0]["Performance_counter"]:
        if events["event_name"] == "sample_filtrate":
            sample_filtrate = events["counter_value"]

    assert sample_filtrate >= 0
    assert samples_dropped >= 0
    assert samples_generated == sample_filtrate
