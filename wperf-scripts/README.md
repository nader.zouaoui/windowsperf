# Scripts

[[_TOC_]]

# How to install all dependencies for Python scripts in this directory

Use the command below to install the packages according to the configuration file `requirements.txt`.

```
> pip install -r requirements.txt
```

# Script library

## Tests

`wperf` regression tests written with Python (and `pytest`). See [README.md](tests/README.md) for more details.

## wperf-devcon PS1 script

This script is wrapper for `devcon` command line tool. See [README.md](devcon/README.md) for more details.

## Script arch_events_update.py

`arch_events_update.py` script can fetch PMU events stored in [ARM-software/data/pmu](https://github.com/ARM-software/data/blob/master/pmu/) and output in format compatible with [armv8-arch-events.def](https://gitlab.com/Linaro/WindowsPerf/windowsperf/-/blob/main/wperf-common/armv8-arch-events.def).

```
> python3 arch_events_update.py
usage: arch_events_update.py [-h] [-l] [-c CPU] [-o OUTPUT] [--license LICENSE]

update the cpu's pmu events file!

options:
  -h, --help            show this help message and exit
  -l, --list_cpu        list all cpus in https://github.com/ARM-software/data/blob/master/pmu/
  -c CPU, --cpu CPU     cpu type that to update
  -o OUTPUT, --output OUTPUT
                        pmu events file for wperf
  --license LICENSE     license file added to the script header
```

## Script telemetry_events_update.py

Script fetches Telemetry Solution CPU's PMU related information from [Telemetry Solution](https://gitlab.arm.com/telemetry-solution/telemetry-solution/-/tree/main/data/pmu/cpu).

Note: for simplicity we now hard-code CPUs available in above repository. In the future we will add enumeration functionality!

### What to do when new Arm Telemetry Solution CPUs is added?

You should add new CPU to `PMU_CPU_MAPPING` map in `telemetry_events_update.py` script.
And run `regenerate-ts-def.sh` script to recreate all `.def` files.

### Script regenerate-ts-def.sh

This script simply calls `telemetry_events_update.py` for each Arm Telemetry CPU and generates separate .def file for each CPU.
The Arm Telemetry Solution `.def` files will be updated in `wperf-common`.

```sh
./regenerate-ts-def.sh
```

### Usage

Print to terminal:

```
> python telemetry_events_update.py
```

To manually pipe into a destination file:

```
> python telemetry_events_update.py > ..\wperf-common\telemetry-solution-data.def
```

`-o` flag, path of desired output file (file will be made or overwritten). If not specified, outputs to stdout. `--output` is an alias.
```
> python telemetry_events_update.py -o ..\wperf-common\telemetry-solution-data.def
```

`--url` and `--file` flags, controls input type which can either be: `url` or `file`. If neither are specified, the default input is 'url'. URL path and local file path are both hardcoded within the script itself.

```
> python telemetry_events_update.py --url
> python telemetry_events_update.py --file
> python telemetry_events_update.py --file --output ./test.def
```

## Script scan-data.sh

This bash script processes JSON files in a specified directory. It looks for files with names starting with "cortex-" and having a ".json" extension.
For  each matching file, it extracts specific fields using the jq command: cpuid, cpu, architecture, and pmu_architecture. The script then performs the following operations:

1.  Splits the cpuid field into the last three characters and the rest.
2.  Removes the suffix "-a" from the architecture field.
3.  Skips printing the details if the architecture field contains the "armv7" prefix.
4.  Prints the extracted fields along with the file name without the extension.
5.  Printouts are in WindowsPerf project macro `WPERF_TS_PRODUCT_CONFIGURATION` format.

### How to use

1. Clone [ARM-Software/data](https://github.com/ARM-software/data/) repository:

```
> git clone git@github.com:ARM-software/data.git
```

2. Go to [ARM-Software/data/pmu](https://github.com/ARM-software/data/tree/master/pmu) directory and execute `scan-data.sh` script.

> Note: Not all cores are included here!

Below output becomes content of `wperf-common/cortex-products.def` file.

```
> cd data/pmu
> ./scan-data.sh .
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a32","armv8",0x41,0,0,0,0,0xd01,"pmuv3","Cortex-A32")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a34","armv8",0x41,0,0,0,0,0xd02,"pmuv3","Cortex-A34")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a35","armv8",0x41,0,0,0,0,0xd04,"pmuv3","Cortex-A35")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a510","armv9",0x41,0,0,0,0,0xd46,"pmuv3","Cortex-A510")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a520","armv9.2",0x41,0,0,0,0,0xd80,"pmuv3","Cortex-A520")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a53","armv8",0x41,0,0,0,0,0xd03,"pmuv3","Cortex-A53")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a55","armv8.2",0x41,0,0,0,0,0xd05,"pmuv3","Cortex-A55")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a57","armv8",0x41,0,0,0,0,0xd07,"pmuv3","Cortex-A57")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a65","armv8",0x41,0,0,0,0,0xd06,"pmuv3","Cortex-A65")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a65ae","armv8",0x41,0,0,0,0,0xd43,"pmuv3","Cortex-A65AE")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a710","armv9",0x41,0,0,0,0,0xd47,"pmuv3","Cortex-A710")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a715","armv9",0x41,0,0,0,0,0xd4d,"pmuv3","Cortex-A715")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a72","armv8",0x41,0,0,0,0,0xd08,"pmuv3","Cortex-A72")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a720","armv9.2",0x41,0,0,0,0,0xd81,"pmuv3","Cortex-A720")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a73","armv8",0x41,0,0,0,0,0xd09,"pmuv3","Cortex-A73")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a75","armv8",0x41,0,0,0,0,0xd0a,"pmuv3","Cortex-A75")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a76","armv8",0x41,0,0,0,0,0xd0b,"pmuv3","Cortex-A76")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a76ae","armv8",0x41,0,0,0,0,0xd0e,"pmuv3","Cortex-A76AE")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a77","armv8.2",0x41,0,0,0,0,0xd0d,"pmuv3","Cortex-A77")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a78","armv8.2",0x41,0,0,0,0,0xd41,"pmuv3","Cortex-A78")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-a78c","armv8.2",0x41,0,0,0,0,0xd4b,"pmuv3","Cortex-A78C")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-x1","armv8.2",0x41,0,0,0,0,0xd44,"pmuv3","Cortex-X1")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-x1c","armv8.2",0x41,0,0,0,0,0xd4c,"pmuv3","Cortex-X1C")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-x2","armv9",0x41,0,0,0,0,0xd48,"pmuv3","Cortex-X2")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-x3","armv9",0x41,0,0,0,0,0xd4e,"pmuv3","Cortex-X3")
WPERF_TS_PRODUCT_CONFIGURATION("cortex-x4","armv9.2",0x41,0,0,0,0,0xd82,"pmuv3","Cortex-X4")
```
