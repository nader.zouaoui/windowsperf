// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <map>
#include <string>
#include "products.h"
#include "cpuinfo.h"


static std::map<std::wstring, struct product_configuration> cpuinfo_product_configuration = {
#define WPERF_TS_EVENTS(...)
#define WPERF_TS_METRICS(...)
#define WPERF_TS_PRODUCT_CONFIGURATION(A,B,C,D,E,F,G,H,I,J) {std::wstring(L##A),{std::wstring(L##B),C,D,E,F,G,H,std::wstring(L##I),std::wstring(L##J)}},
#define WPERF_TS_ALIAS(...)
#define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n1.def"
#include "wperf-common/neoverse-n2-r0p0.def"
#include "wperf-common/neoverse-n2-r0p1.def"
#include "wperf-common/neoverse-n2-r0p3.def"
#include "wperf-common/neoverse-n2.def"
#include "wperf-common/neoverse-n3.def"
#include "wperf-common/neoverse-v1.def"
#include "wperf-common/neoverse-v2.def"
#include "wperf-common/neoverse-v3.def"
#include "wperf-common/cortex-products.def"	
#undef WPERF_TS_EVENTS
#undef WPERF_TS_METRICS
#undef WPERF_TS_PRODUCT_CONFIGURATION
#undef WPERF_TS_ALIAS
#undef WPERF_TS_GROUPS_METRICS
};

/// <summary>
/// Retrieves the product name of a CPU based on its implementer and part number.
/// </summary>
/// <param name="implementer">The implementer ID of the CPU.</param>
/// <param name="part_num">The part number of the CPU.</param>
/// <param name="name">A reference to a std::wstring where the product name will be stored if found.</param>
/// <returns>Returns true if the product name is found; otherwise, returns false.</returns>
/// <remarks>
/// This function searches the global cpuinfo_product_configuration map for a matching implementer and part number.
/// If a match is found, the corresponding product name is assigned to the <paramref name="name"/> parameter.
/// If no match is found, <paramref name="name"/> is set to an empty string.
/// </remarks>
bool cpuinfo_get_product_name(uint8_t implementer, uint16_t part_num, std::wstring& name)
{
    for (const auto& [prod_name, prod_conf] : cpuinfo_product_configuration) {
        if (prod_conf.implementer == implementer &&
            prod_conf.part_num == part_num) {
            name = prod_name;
            return true;
        }
    }
    name = std::wstring();
    return false;
}

/// <summary>
/// Retrieves the product configuration of a CPU based on its implementer and part number.
/// </summary>
/// <param name="implementer">The implementer ID of the CPU.</param>
/// <param name="part_num">The part number of the CPU.</param>
/// <param name="pc">A reference to a <c>product_configuration</c> structure where the matching configuration will be stored if found.</param>
/// <returns>Returns true if the product configuration is found; otherwise, returns false.</returns>
/// <remarks>
/// This function searches the global <c>cpuinfo_product_configuration</c> map for a matching implementer and part number.
/// If a match is found, the corresponding product configuration is assigned to the <paramref name="pc"/> parameter.
/// If no match is found, the <paramref name="pc"/> structure remains unchanged.
/// </remarks>
bool cpuinfo_get_product_configuration(uint8_t implementer, uint16_t part_num, struct product_configuration& pc) 
{
    for (const auto& [prod_name, prod_conf] : cpuinfo_product_configuration) {
        if (prod_conf.implementer == implementer &&
            prod_conf.part_num == part_num) {
            pc = prod_conf;
            return true;
        }
    }
    return false;
}
