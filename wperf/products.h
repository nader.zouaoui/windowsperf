#pragma once
// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <string>

/*
* Data structures describing Arm Telemetry Solution "product", in short an Arm CPU product.
*/

struct product_alias
{
    std::wstring alias;
    std::wstring name;
};

struct product_configuration
{
    std::wstring arch_str;
    uint8_t implementer{};
    uint8_t major_revision{};
    uint8_t minor_revision{};
    uint8_t num_bus_slots{};
    uint8_t num_slots{};
    uint16_t part_num{};
    std::wstring pmu_architecture;
    std::wstring product_name;
};

struct product_event
{
    std::wstring name;                  // Event name
    uint16_t index{};                   // Event index
    std::wstring title;                 // Event title / short description
    std::wstring description{ L"n/a" };   // Event long description
};

struct product_metric
{
    std::wstring name;              // :^)
    std::wstring events_raw;        // Raw string of events, comma separated e.g. "cpu_cycles,stall_backend"
    std::wstring metric_formula;    // Raw string with metric formula, e.g. "((STALL_BACKEND / CPU_CYCLES) * 100)"
    std::wstring metric_formula_sy; // Raw string with RPN equivalent expression of metric formula e.g. "STALL_BACKEND CPU_CYCLES / 100 *"
    std::wstring metric_unit;       // Raw string with metric unit "percent of cycles"
    std::wstring title;             // Metric title / short description
    std::wstring description;       // Metric longer description
};

struct product_group_metrics
{
    std::wstring name;              // :^)
    std::wstring metrics_raw;       // Raw string of metrics, comma separated e.g. "l2_cache_mpki,l2_cache_miss_ratio"
    std::wstring title;             // Metric title / short description
    std::wstring description;       // Metric Group description
};