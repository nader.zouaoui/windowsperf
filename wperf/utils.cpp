// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <string>
#include <sstream>
#include <stringapiset.h>
#include <vector>
#include <cwctype>
#include <unordered_map>
#include <numeric>
#include "utils.h"

/// <summary>
/// Tokenizes wstring to a vector of wstring tokens delimeted by a specific character.
/// </summary>
/// <param name="str">Source string to get the tokens</param>
/// <param name="delim">The delimiting character</param>
/// <param name="tokens">The vector that is going to receive the tokens</param>
void TokenizeWideStringOfStrings(const std::wstring& str, const wchar_t& delim, std::vector<std::wstring>& tokens)
{
    using size_type = std::basic_string<wchar_t>::size_type;
    size_type pos = 0, last_pos = 0;
    pos = str.find(delim);
    while (pos != std::basic_string<wchar_t>::npos)
    {
        if (pos != last_pos)
        {
            std::wstring token = str.substr(last_pos, pos - last_pos);
            tokens.push_back(token);
        }
        last_pos = pos + 1;
        pos = str.find(delim, last_pos);
    }
    if (last_pos < str.size())
    {
        tokens.push_back(str.substr(last_pos, str.size() - last_pos + 1));
    }
}

/// <summary>
/// Tokenizes wstring to a vector of wstring tokens delimeted by a specific character. Includes delimiter in token.
/// 
/// For example, splitting a sentence on a comma, where you would like to preserve the comma.
/// </summary>
/// <param name="str">Source string to get the tokens</param>
/// <param name="delim">The delimiting character</param>
/// <param name="tokens">The vector that is going to receive the tokens</param>
void TokenizeWideStringOfStringsDelim(const std::wstring& str, const wchar_t& delim, std::vector<std::wstring>& tokens)
{
    using size_type = std::basic_string<wchar_t>::size_type;
    size_type pos = 0, last_pos = 0;
    pos = str.find(delim);
    while (pos != std::basic_string<wchar_t>::npos)
    {
        std::wstring token = str.substr(last_pos, pos - last_pos);
        token += delim;
        tokens.push_back(token);

        last_pos = pos + 1;
        pos = str.find(delim, last_pos);
    }
    if (last_pos < str.size())
    {
        tokens.push_back(str.substr(last_pos, str.size() - last_pos + 1));
    }
}


std::string MultiByteFromWideString(const wchar_t* wstr)
{
    if (!wstr)
        return std::string();
    int required_size = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);
    std::shared_ptr<char[]> str_raw(new char[required_size]);
    WideCharToMultiByte(CP_ACP, 0, wstr, -1, str_raw.get(), required_size, NULL, NULL);
    return std::string(str_raw.get());
}

std::wstring TrimWideString(const std::wstring& wstr)
{
    if (wstr.size() == 0)
        return wstr;

    std::wstring::size_type i = 0, j = wstr.size()-1;
    
    while (i < wstr.size() && std::iswspace(wstr[i])) i++;
    while (j > 0 && std::iswspace(wstr[j])) j--;

    return wstr.substr(i, (j - i) + 1);
}

/// <summary>
/// Converts an array of chars into a wstring.
/// </summary>
/// <param name="str">Source char array to be converted</param>
std::wstring WideStringFromMultiByte(const char* str)
{
    if (!str)
        return std::wstring();
    int required_size = MultiByteToWideChar(CP_UTF8, 0, str, -1, NULL, 0);
    std::shared_ptr<wchar_t[]> wstr_raw(new wchar_t[required_size]);
    MultiByteToWideChar(CP_UTF8, 0, str, -1, wstr_raw.get(), required_size);
    return std::wstring(wstr_raw.get());
}

/// <summary>
/// Converts double to WSTRING. Function is using fixed format
/// and default precision set to 2.
/// </summary>
/// <param name="Value">Value to convert</param>
/// <param name="Precision">Precision</param>
std::wstring DoubleToWideString(double Value, int Precision) {
    std::wstringstream ss;
    ss << std::fixed << std::setprecision(Precision) << Value;
    return std::wstring(ss.str());
}

/// <summary>
/// Converts double to WSTRING. Function is using fixed format
/// and default precision set to 2.
/// </summary>
/// <param name="Value">Value to convert</param>
/// <param name="Precision">Precision</param>
/// <param name="Width">Total string width</param>
std::wstring DoubleToWideStringExt(double Value, int Precision, int Width) {
    std::wstringstream ss;
    ss << std::fixed << std::setw(Width) << std::setprecision(Precision) << Value;
    return std::wstring(ss.str());
}

std::wstring ReplaceFileExtension(std::wstring filename, std::wstring ext)
{
    size_t index = filename.find_last_of(L".");
    if (index != std::string::npos)
        filename = filename.substr(0, index) + L"." + ext;
    return filename;
}

/// <summary>
/// Case insensitive WSTRINGs comparision.
/// </summary>
/// <param name="str1">WSTRING to compare</param>
/// <param name="str2">WSTRING to compare</param>
bool CaseInsensitiveWStringComparision(const std::wstring& str1, const std::wstring& str2)
{
    return std::equal(str1.begin(), str1.end(),
        str2.begin(), str2.end(),
        [](wchar_t a, wchar_t b) { return towlower(a) == towlower(b); });
}

/// <summary>
/// Converst WSTRING to its lowercase counterpart
/// </summary>
/// <param name="str">WSTRING to lowercase</param>
std::wstring WStringToLower(const std::wstring& str)
{
    std::wstring result;
    result.resize(str.size());
    std::transform(str.begin(), str.end(), result.begin(), towlower);
    return result;
}

/// <summary>
/// Join WSTRINGs frm input vector
/// </summary>
/// <param name="str">Joined with SEP WSTRING</param>
std::wstring WStringJoin(const std::vector<std::wstring>& input, std::wstring sep)
{
    std::wstring first = input.size() ? input[0] : std::wstring();

    if (input.size() <= 1)
        return first;

    return std::accumulate(input.begin() + 1, input.end(), first,
        [&sep](const std::wstring& a, const std::wstring& b) -> std::wstring {
            return a + sep + b;
        });
}

/// <summary>
/// Return TRUE if STR starts with PREFIX
/// </summary>
/// <param name="str">String to compare</param>
/// <param name="prefix">Prefix to compare</param>
bool WStringStartsWith(const std::wstring& str, const std::wstring& prefix)
{
    return (str.size() >= prefix.size() &&
        std::equal(prefix.begin(), prefix.end(), str.begin()));
}

/// <summary>
/// Return TRUE if STR ends with SUFFIX
/// </summary>
/// <param name="str">String to compare</param>
/// <param name="prefix">Prefix to compare</param>
bool WStringEndsWith(const std::wstring& str, const std::wstring& suffix)
{
    return (str.size() >= suffix.size() &&
        std::equal(suffix.rbegin(), suffix.rend(), str.rbegin()));
}

/// <summary>
/// Return TRUE if STR starts with PREFIX (case insensitive comparision)
/// </summary>
/// <param name="str">String to compare</param>
/// <param name="prefix">Prefix to compare</param>
bool CaseInsensitiveWStringStartsWith(const std::wstring& str, const std::wstring& prefix)
{
    return WStringStartsWith(WStringToLower(str), WStringToLower(prefix));
}

/// <summary>
/// Return TRUE if STR starts with PREFIX (case insensitive comparision)
/// </summary>
/// <param name="str">String to compare</param>
/// <param name="prefix">Prefix to compare</param>
bool CaseInsensitiveWStringEndsWith(const std::wstring& str, const std::wstring& suffix)
{
    return WStringEndsWith(WStringToLower(str), WStringToLower(suffix));
}

/// <summary>
/// Replaces OLD_TOKEN with NEW_TOKEN
/// </summary>
/// <param name="input">Input string with OLD_TOKEN to replace</param>
/// <param name="old_token">Token to replace</param>
/// <param name="new_token">Replaces OLD_TOKEN in INPUT</param>
/// <returns>TRUE if OLD_TOKEN was replaced with NEW_TOKEN returns TRUE</returns>
bool ReplaceTokenInString(std::string& input, const std::string old_token, const std::string new_token)
{
    bool result = true;
    auto pos = input.find(old_token);

    if (pos != std::string::npos)
        input.replace(pos, old_token.size(), new_token);
    else
        result = false;

    return result;
}

/// <summary>
/// Convert time from one of [milliseconds, seconds, minutes, hours, days] to seconds
/// </summary> 
/// <param name="number">Input number to be converted</param>
/// <param name="unit">Unit of input number</param>
/// <param name="unitConversionMap">Map of wstring unit multiplier</param>
double ConvertNumberWithUnit(double number, std::wstring unit, const std::unordered_map<std::wstring, double>& unitConversionMap)
{
    //takes a number and a unit, multiplies the number by a value to obtain number in desired units
    auto it = unitConversionMap.find(unit);
    double multiplier = it->second;
    double result = number * multiplier;

    return result;
}

/// <summary>
/// Replace all OLD_TOKEN occurances with NEW_TOKEN
/// </summary>
/// <param name="str">Input to be converted</param>
/// <param name="old_token">What to replace</param>
/// <param name="new_token">What to replace with</param>
void ReplaceAllTokensInWString(std::wstring& str, const std::wstring& old_token, const std::wstring& new_token)
{
    size_t pos = 0;

    while ((pos = str.find(old_token, pos)) != std::wstring::npos)
    {
        str.replace(pos, old_token.size(), new_token);
        pos += new_token.size();
    }
}

/// <summary>
/// Merge directory name with file name and form full file path
/// Note: only WSTRING path support for Windows OS.
/// </summary>
/// <param name="dir_str">Prefix directory</param>
/// <param name="filename_str">filename to merge</param>
/// <returns>Merged path to the file</returns>
std::wstring GetFullFilePath(std::wstring dir_str, std::wstring filename_str)
{
    std::filesystem::path dir(dir_str);
    std::filesystem::path file(filename_str);
    std::filesystem::path full_path = dir / file;
    return full_path;
}
/// <summary>
/// Splits a wstring into two parts: a numeric (double) portion and a suffix.
/// </summary>
/// <param name="str">
/// The input wstring that contains the numeric portion and optional suffix.
/// </param>
/// <param name="number">
/// [out] A reference to a double that receives the parsed numeric value.
/// </param>
/// <param name="suffix">
/// [out] A reference to a std::wstring that receives the remainder (suffix) of the string
/// after extracting the numeric portion.
/// </param>
/// <returns>
/// Returns `true` if the numeric portion is successfully parsed; otherwise, returns `false`.
/// </returns>
bool SplitDoubleAndSuffixFromWString(const std::wstring& str, double& number, std::wstring& suffix)
{
    int i = 0;
    for (; i < str.size(); i++)
    {
        if (!std::isdigit(str[i]) && (str[i] != L'.')) {
            break;
        }
    }

    std::wstring number_wstring = str.substr(0, i);

    try {
        number = std::stod(number_wstring);
    }
    catch (...) {
        return false;
    }

    suffix = str.substr(i);

	return true;
}

bool CheckerFuncIsNotEmpty(const std::wstring& str)
{
	return !str.empty() && !TrimWideString(str).empty();
}

/// <summary>
/// Checks if a wstring represents an integer (including optional leading '-' sign).
/// </summary>
/// <param name="str">
/// The input wstring to analyze.
/// </param>
/// <returns>
/// Returns `true` if the string contains a non empty string of digits optionally leading with '-';
/// otherwise, returns `false`.
/// </returns>
bool CheckerFuncIsInteger(const std::wstring& str) {
    std::wstring trimmedStd = TrimWideString(str);
	if (trimmedStd.empty()) return false;
	if (trimmedStd.front() == L'-') {
		trimmedStd = trimmedStd.substr(1);
	}
    return !std::any_of(trimmedStd.begin(), trimmedStd.end(), [](wchar_t c) { return !std::isdigit(c); });
}

/// <summary>
/// Checks if the provided wstring is non-empty, represents a valid integer, and is greater than or equal to zero.
/// </summary>
/// <param name="str">
/// The input wstring to analyze.
/// </param>
/// <returns>
/// Returns `true` if the string is not empty, contains only digits (optionally preceded by a '-') and its parsed integer value is >= 0; 
/// otherwise, returns `false`.
/// </returns>
bool CheckerFuncIsPositiveInteger(const std::wstring& str)
{
    if (!CheckerFuncIsNotEmpty(str) || !CheckerFuncIsInteger(str)) return false;

    try
    {
        int out;
        if(!ConvertWStringToInt(str, out, 10)) return false;
        return out >= 0;
    }
    catch (const std::exception&)
    {
        return false;
    }
}

/// <summary>
/// Checks if the input wide string is a non-empty, comma-separated list of non-negative integers.
/// </summary>
/// <param name="str">
/// The input wide string to analyze.
/// </param>
/// <returns>
/// Returns `true` if `str` is not empty, tokenizes successfully into integers (using comma as a delimiter),
/// and all tokens are greater than or equal to zero; otherwise, returns `false`.
/// </returns>
bool CheckerFuncIsCommaSeperatedListOfPositiveNumbers(const std::wstring& str)
{
    if (!CheckerFuncIsNotEmpty(str)) return false;
    std::vector<int> tokens;
	if(!TokenizeWideStringOfInts(str, L',', tokens)) return false;
	for (const auto& token : tokens)
	{
		if (token < 0)
			return false;
	}
	return true;
}

/// <summary>
/// Checks if the provided wide string represents a non-negative numeric value (optionally decimal with up to two decimals)
/// followed by an optional valid time unit ("s", "m", "ms", "h", "d"). If no time unit is provided then the functino defaults to seconds.
/// </summary>
/// <param name="str">
/// The input wide string to analyze.
/// </param>
/// <returns>
/// Returns `true` if the string matches the specified numeric/time-unit pattern and the optional suffix
/// corresponds to a recognized time unit or empty ("s", "m", "ms", "h", "d").
/// Otherwise, returns `false`.
/// </returns>
bool CheckerFuncIsNumberWithTimeUnit(const std::wstring& str)
{
    std::wstring accept_units;

    for (const auto& pair : TimeUnitUnorderedMap)
    {
        if (!accept_units.empty()) {
            accept_units += L"|";
        }
        accept_units += pair.first;
    }

    std::wstring regex_string = L"^(0|([1-9][0-9]*))(\\.[0-9]{1,2})?(" + accept_units + L")?$";
    std::wregex r(regex_string);

    std::wsmatch match;
    if (!std::regex_search(str, match, r)) return false;
    double number;
    std::wstring suffix;

	if (!SplitDoubleAndSuffixFromWString(str, number, suffix)) return false;

    //default to seconds if unit is not provided
    if (suffix.empty()) {
        return true;
    }

    //check if the unit exists in the map
    auto it = TimeUnitUnorderedMap.find(suffix);
    if (it == TimeUnitUnorderedMap.end())
    {
		return false;
    }
    //Note: This exception should never be reached, as it should be caught in the regex construction of check_timeout_arg
    //However, if the unit map/regex construction is changeed in the future, this serves as a good safety net

    return true;
}
