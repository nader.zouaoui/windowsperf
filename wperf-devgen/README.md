# wperf-devgen

[[_TOC_]]

# Introduction

The `wperf-devgen` project is a utility designed to facilitate the installation of the WindowsPerf Kernel Driver through the Software Device API.

# Requirements

Before running the executable make sure `wperf-driver.sys`, `wperf-driver.inf` and `wperf-driver.cat` are
all at the same directory as the `wperf-devgen` executable. Make sure those file are properly signed by Linaro and Microsoft.

> :warning: `wperf-devgen` requires latest Microsoft Visual C++ Redistributable version. If you encounter a `Runtime Error` (A Windows Error 0xC000007B) or a `System Error` (VCRUNTIME140.dll and MSVCP140.dll missing) during tool execution, please download and install the [latest Microsoft Visual C++ Redistributable Version](https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170#latest-microsoft-visual-c-redistributable-version).

# Usage

You can type `wperf-devgen install` to install the software device along with the driver. If some error occurs and
the device and driver were already installed make sure to type `wperf-devgen uninstall` or `wperf-devgen reinstall` to remove the device first.
`reinstall` acts as `uninstall` followed by `install`.

`wperf-devgen enable` and `wperf-devgen disable` may be used to enable or disable the driver without removing it.
`wperf-devgen restart` acts as `disable` followed by `enable`.

## Driver Installation

```
> wperf-devgen.exe install
Found path C:\path\to\wperf-driver\wperf-driver.inf
Executing command: install.
Install requested.
Device installed successfully
```

## Driver Uninstallation

```
> wperf-devgen uninstall
Found path C:\path\to\wperf-driver\wperf-driver.inf
Executing command: uninstall.
Uninstall requested.
Root\WPERFDRIVER
Device found
Device uninstalled successfully
```

## Driver Reinstallation

```
> wperf-devgen reinstall
Found path C:\path\to\wperf-driver\wperf-driver.inf
Executing command: reinstall.
Reinstall requested.
Root\WPERFDRIVER
Device found
Device uninstalled successfully
Device installed successfully
```

## Toggling an installed driver

```
> wperf-devgen enable
Found path C:\path\to\wperf-driver\wperf-driver.inf
Executing command: enable.
Enable requested.
Root\WPERFDRIVER
Device found
Device enabled successfully
```

```
> wperf-devgen disable
Found path C:\path\to\wperf-driver\wperf-driver.inf
Executing command: disable.
Disable requested.
Root\WPERFDRIVER
Device found
Device disabled successfully
```

```
> wperf-devgen restart
Found path C:\path\to\wperf-driver\wperf-driver.inf
Executing command: restart.
Restart requested.
Root\WPERFDRIVER
Device found
Device disabled successfully
Root\WPERFDRIVER
Device found
Device enabled successfully
```
